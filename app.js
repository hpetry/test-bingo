const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);

keywords=[
    "AWS",
    "EC2",
    "EKS",
    "ECR",
    "ECS",
    "T2MICRO",
    "MURTY",
    "TEJAS",
    "ROHIT",
    "KUNAL",
    "MIRO",
    "MATT",
    "HISHAM",
    "MIKE",
    "SPLUNK",
    "KIBANA",
    "ALB",
    "NLB",
    "WAF",
    "AKAMAI",
    "SYDNEYCARE",
    "APACHE",
    "NGINX",
    "JIRA",
    "EPIC",
    "STORY",
    "TASK",
    "CONFLUENCE",
    "BAMBOO",
    "BRANCH",
    "MERGE",
    "IDENTITY",
    "DATADOG",
    "SSH",
    "DOCKER",
    "CONTAINER",
    "BITBUCKET",
    "CICD"
];


app.get('/', function(req, res) {
    res.render('index.ejs');
});

app.get('/testpage', function(req, res) {
    res.render('test.ejs');
});

app.get('/controller', function(req, res) {
    res.render('controller.ejs');
});


setInterval(tenSecondFunction, 10000);

function tenSecondFunction() {
    try {
	io.emit('keywords', JSON.stringify(keywords));
    }
    catch(err) {
	console.log(err);
    }
}

var tkeywords=[]
function reset() {
    tkeywords=[];
    for (i=0 ; i < keywords.length ; i++) {
	tkeywords.push(keywords[i]);
    }
    console.log(tkeywords);
}

function getkeyword() {
    var randomItem = tkeywords[Math.floor(Math.random()*tkeywords.length)];
    var tarr=[];
    for (i=0 ; i < tkeywords.length ; i++) {
	if (tkeywords[i] != randomItem) {
	    tarr.push(tkeywords[i]);
	}
    }
    tkeywords=tarr;
    return randomItem;
}

reset();
io.sockets.on('connection', function(socket) {
    socket.on('sendreset', function() {
	console.log("received sendreset");
	reset();
        io.emit('reset', '{}');
    });

    socket.on('sendkeyword', function() {
	console.log("received sendkeyword");
        io.emit('keyword', getkeyword());
    });

    socket.on('*',function(event, data) {
	console.log('Event received: ' + event + ' - data:' + JSON.stringify(data));
    });
    
});

const server = http.listen(3000, function() {
    console.log('listening on *:3000');
});

module.exports = app;
